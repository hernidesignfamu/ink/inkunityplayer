VAR inventar_kotlik = false
VAR inventar_kotlik_s_vodou = false
VAR inventar_zalud = false
VAR kotlik_na_ohni = false

Kdysi v jednom stanu, pospával poutník a chrápal.
-> Stan.VeStanu


=== Stan ===
Poutník přišel ke stanu. <>
    -> PredStanem

= PredStanem
Postával před stanem a pozoroval ptáky na stromech. {Kaluz: Byl tak od bláta, že lézt do stanu nepřipadalo v úvahu.}
    -> Vyber

= Vyber
* [Vyplašit ptáky]
    Vyplašil křikem ptáky na stromech. Ti se po chvíli ale vrátili na svá místa.
    -> Vyber
+ {Kaluz or VeStanu < 3}[Zalézt do stanu]
    Poutník {podruhé|potřetí|opět} zalezl do stanu a začal chrápat. {not Mapa: Zdálo se mu o mapě, kterou má v batohu.}
    -> VeStanu
+ {not inventar_zalud} [Sebrat žalud]
    Poutník sebral žalud a dal si ho do batohu.
    ~ inventar_zalud = true
    -> Vyber
+ [Prohlédnout si mapu]
    -> Mapa
    
= VeStanu
+{not PredStanem} [Vylézt ze stanu]
    -> VyleztZeStanu
+{PredStanem} [Znovu vylézt ze stanu]
    -> VyleztZeStanu
    
= VyleztZeStanu
Probudil se. {&Bylo pondělí|Bylo úterý|Byla středa|Byl čtvrtek|Byl pátek}. Vylezl ze stanu a protáhl se.
{not PredStanem: Z batohu vytáhl mapu.}
->PredStanem


=== Prevoznik ===
Poutník{Kaluz:, celý od bláta z louže,} dorazil na břeh řeky, kde se na dlouhé dřevěné loďce kolébal převozník. Pospával, opřený o své bidlo.
    -> Vyber

= Vyber
+ (DobryDen) "Dobrý den, mohl bych se s vámi svézt?" []Oslovil ho poutník.
+ (HejTy)"Hej ty ospalče!" []Křikl na něj poutník.
+ {inventar_kotlik} [Nabrat vodu do kotlíku]
    -> NabratVodu
+ [Prohlédnout si mapu]
    -> Mapa

- 
{DobryDen: Převozník zvedl zvolna hlavu, vlídně se podíval na poutníka }
{HejTy: Převozník se polekal kdo to na něj řve, mrzutě se podíval na poutníka }
<>a začal na něj chrlit nepravděpodobnou historku o tom jak mu kamsi uletěl jeho kamarád ptáček.
    Zdá se, že bez ptáčka převozník nikam nepojede.
    A tak se poutník vydal hledat ptáčka.
    -> Mapa

= NabratVodu
Poutník se sehnul a nabral vodu z břehu řeky. Udělal to tak potichu, že si ho převozník ani nevšimnul.
    ~ inventar_kotlik = false
    ~ inventar_kotlik_s_vodou = true
    -> Vyber


=== Kaluz ===
* [Napít se z louže]
    Napil se z bahnité louže. Moc Dobře mu to neudělalo.
    ->Kaluz
* [Nabrat bláto do kapes]
    Začal si cpát bláto do kapes. "K čemu mi může bejt dobrý bláto v kapsách" říkal si, když už byl úplně celej zadělanej.
    -> Kaluz
* [Podívat se do louže]
    V louži se hemžila hromada pulců. Mezi mrskajícími se ocásky viděl poutník odraz svého obličeje.
    -> Kaluz
* ->
    Zdálo se, že u louže už si poutník o moc víc legrace neužije. A tak vytáhl z kapsy mapu a rozmýšlel kam se vydat dál.
    -> Mapa

=== Taboriste ===
V táboře hořel oheň. <>
{not inventar_kotlik and not inventar_kotlik_s_vodou and not kotlik_na_ohni:Vedle ohniště se válel prázdný kotlík.}
{kotlik_na_ohni:Na ohni bublal kotlík s vodou.}
-> Vyber

= Vyber
+ {not inventar_kotlik and not inventar_kotlik_s_vodou and not kotlik_na_ohni} [Sebrat kotlík] Poutník sebral kotlík ze země a dal si ho do batohu.
    ~ inventar_kotlik = true
    -> Vyber
+ {inventar_kotlik} [Dát kotlík na oheň] Poutník vyndal z batohu kotlík a dal ho na oheň. Ale s prázdným kotlíkem to nemělo valný smysl. Měl by někde sehnat vodu. A tak opět strčil kotlík do batohu.
    -> Vyber
+ {inventar_kotlik_s_vodou} [Dát kotlík s vodou na oheň] Poutník dal na oheň vařit kotlík s vodou. Po chvilce začala voda v kotlíku bublat.
    ~ inventar_kotlik_s_vodou = false
    ~ kotlik_na_ohni = true
    -> Vyber
+ [Prohlédnout si mapu]
    -> Mapa

=== Batoh ===
V batohu má: Mapu{inventar_kotlik:, kotlík}{inventar_kotlik_s_vodou:, kotlík s vodou}{inventar_zalud:, žalud}.
    -> Vyber
= Vyber
+ {inventar_kotlik} [Prohlédnout kotlík]
    Potník si prohlédl plechový kotlík na vaření na ohni.
    -> Vyber
+ {inventar_kotlik_s_vodou} [Prohlédnout kotlík s vodou]
    Poutník si prohlédl plechový kotlík plný vody. Zajímavé je, že se v batohu voda nevylila. Opatrně ho vložil zpět do batohu.
    -> Vyber
+ {inventar_zalud} [Prohlédnout žalud]
    Poutník si prohlédl žalud.
    "Možná by se s ním dalo střílet z bambitky", pomyslel si.
    -> Vyber
+ [Prohlédnout si mapu]
    -> Mapa


=== Mapa ===
+ [Prohledat zbytek batohu]
    Poutník prohledal celý batoh.
    -> Batoh
    
+ [Jít k tábořišti]
    Poutník se vydal k tábořišti.
    -> Taboriste
+ [Jít ke stanu]
    Poutník se vydal ke stanu.
    -> Stan
+ {not Kaluz} [Jít ke kaluži]
    Poutník přišel k velké zablácené kaluži.
    -> Kaluz
+ [Jít k převozníkovi]
    Poutník se vydal k převozníkovi.
    -> Prevoznik
+ [Toulat se]
    Poutník se toulal krajinou.
    {~->Stan|->Prevoznik|->Taboriste}