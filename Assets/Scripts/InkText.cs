using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class InkText : MonoBehaviour
{
	public void Destroy()
	{
		var texts = GetComponentsInChildren<Text>();
		for (var i = 0; i < texts.Length; i++)
		{
			texts[i].DOColor(Color.grey, 1f);
		}
	}
}
