using UnityEngine;
using Ink.Runtime;

public class InkManipulator : MonoBehaviour
{
	private const string MyVariableName = "inventar_zalud";
	
	public InkController inkController;
	public Camera gameCamera;
	public SpriteRenderer spriteRenderer;
	public Sprite sprite1;
	public Sprite sprite2;

	private Story Story => inkController.story;
	
	private void Start()
	{
		// Observe variable and made changes when variable is changed
		Story.ObserveVariable(MyVariableName, (varName, newValue) => {
			Debug.Log($"{varName}, new value: {newValue}");

			// Cast type from object to specific type of variable (bool in this case). We have to know this from Ink and make it manually.
			var myVariableValue = (bool) newValue;
			
			// Example - manipulate the game view based on new variable value
			gameCamera.backgroundColor = myVariableValue ? Color.black : Color.white;
		});
	}

	private void Update()
	{
		// Get variable state
		// Cast to specific type (as above, in this case also bool)
		var myVariableState = (bool) Story.variablesState[MyVariableName];
		
		// Example - update game view in each frame based on the variables
		spriteRenderer.sprite = myVariableState ? sprite1 : sprite2;
		
		// Set variable state
		// Example - set variables eg. based on user input (pressing A button in this case)
		if (Input.GetKeyDown(KeyCode.A))
			Story.variablesState[MyVariableName] = false;
		
		// Example - toggle variable based on input (pressing B)
		if (Input.GetKeyDown(KeyCode.B))
			Story.variablesState[MyVariableName] = !(bool)Story.variablesState[MyVariableName];
		
		// Example - jump to knot named "Map" when pressing M
		if (Input.GetKeyDown(KeyCode.M))
		{
			Story.ChoosePathString("Mapa");
			inkController.RefreshView();
		}

	}
}
