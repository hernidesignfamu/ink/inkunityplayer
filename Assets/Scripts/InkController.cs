﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Ink.Runtime;

// This is a super bare bones example of how to play and display a ink story in Unity.
public class InkController : MonoBehaviour 
{
	[SerializeField] private TextAsset inkJSONAsset;
	[SerializeField] private Transform content;
	[SerializeField] private ScrollRect scrollRect;

	// UI Prefabs
	[SerializeField] private Text textPrefab;
	[SerializeField] private Button buttonPrefab;
	public Story story;
	
	public static event Action<Story> OnCreateStory;

	private void Awake () 
	{
		// Remove the default message
		RemoveChildren();
		StartStory();
	}

	// Creates a new Story object with the compiled story which we can then play!
	private void StartStory () 
	{
		story = new Story (inkJSONAsset.text);

		if (OnCreateStory != null) OnCreateStory(story);
		RefreshView();
	}

	// This is the main function called every time the story changes. It does a few things:
	// Destroys all the old content and choices.
	// Continues over all the lines of text, then displays all the choices. If there are no choices, the story is finished!
	public void RefreshView () 
	{
		// Remove all the UI on screen
		RemoveChildren ();
		
		// Read all the content until we can't continue any more
		while (story.canContinue) {
			// Continue gets the next line of the story
			string text = story.Continue ();
			// This removes any white space from the text.
			text = text.Trim();
			// Display the text on screen!
			CreateContentView(text);
		}

		// Display all the choices, if there are any!
		if(story.currentChoices.Count > 0) {
			for (int i = 0; i < story.currentChoices.Count; i++) {
				Choice choice = story.currentChoices [i];
				Button button = CreateChoiceView (choice.text.Trim ());
				// Tell the button what to do when we press it
				button.onClick.AddListener (delegate {
					OnClickChoiceButton (choice);
				});
			}
		}
		// If we've read all the content and there's no choices, the story is finished!
		else {
			Button choice = CreateChoiceView("End of story.\nRestart?");
			choice.onClick.AddListener(delegate{
				StartStory();
			});
		}
		
		Invoke(nameof(DelayedVerticalRefresh), 0.1f);
	}

	// When we click the choice button, tell the story to choose that choice!
	private void OnClickChoiceButton (Choice choice) 
	{
		story.ChooseChoiceIndex (choice.index);
		RefreshView();
	}

	// Creates a textbox showing the the line of text
	private void CreateContentView (string text) 
	{
		Text storyText = Instantiate (textPrefab) as Text;
		storyText.text = text;
		storyText.transform.SetParent (content, false);
	}

	// Creates a button showing the choice text
	private Button CreateChoiceView (string text) 
	{
		// Creates the button from a prefab
		Button choice = Instantiate (buttonPrefab) as Button;
		choice.transform.SetParent (content, false);
		
		// Gets the text from the button prefab
		Text choiceText = choice.GetComponentInChildren<Text> ();
		choiceText.text = text;

		// Make the button expand to fit the text
		HorizontalLayoutGroup layoutGroup = choice.GetComponent <HorizontalLayoutGroup> ();
		layoutGroup.childForceExpandHeight = false;

		return choice;
	}

	// Destroys all the children of this gameobject (all the UI)
	private void RemoveChildren () 
	{
		int childCount = content.childCount;
		for (int i = childCount - 1; i >= 0; --i)
		{
			var inkText = content.GetChild(i).GetComponent<InkText>();
			if (inkText != null)
				inkText.Destroy();
			else	
				Destroy (content.GetChild (i).gameObject);
		}
	}

	private void DelayedVerticalRefresh()
	{
		scrollRect.verticalNormalizedPosition = 0f;
	}
}
